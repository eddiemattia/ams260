import matplotlib.pyplot as plt
import numpy as np

method = "FOG"
N = 32
xa = 0.0
xb = 1.0
ul = 1
ur = -1
xd = 0.5
dx = (xb-xa)/N
C = 0.9
dt = C*dx/2
T = 0.1
M = 144
write_every=1
iters = [0]
i = write_every
while i <= M:
    iters.append(i)
    i+=write_every
data_to_plot = []

timeFilename = '../PDE/data/output_{0}_timeDiscretization.dat'.format(method)
print(timeFilename)
timeFilename = '%r'%timeFilename
print(timeFilename)
timeFilename = timeFilename[1:-1]
print(timeFilename)
timeFileObj = open(timeFilename, 'r')
times = [] # col0 -> dts, col1 -> ElapsedTime

for i in iters:
    pair = timeFileObj.readline().split()
    times.append(np.array([pair[0], pair[1]]))
    filename = '../PDE/data/output_{0}_time{1}.dat'.format(method,i)
    filename = '%r'%filename
    filename = filename[1:-1]
    print("\t READING FROM: {0}".format(filename))
    file_obj = open(filename, 'r')
    method_name = filename.split('.')[2].split('_')[1]
    it = int(filename.split('.')[2].split('time')[-1])
    binary=False
    if binary:
        pass
    else:
        data = file_obj.readlines()
        data = np.fromstring(' '.join(data[1:]), dtype=float, sep=' ')
        x = []
        x.append(xa-1.5*dx)
        for i in range(1, len(data[1:])):
            x.append(x[i-1] + dx)
        data_to_plot.append(data[1:])

times = np.array(times)
print("len(x)={} | len(times)={}".format(len(x), len(times)))

for timeStep in range(len(data_to_plot)):
    if timeStep in [8]: 
        plt.plot(x[2:-1],data_to_plot[timeStep][1:-2], 'ro:',
            label='$t$ = {0}$\Delta$t={1}'.format(timeStep, round(float(times[timeStep,1]),3)))

    elif timeStep in [20]:
        plt.plot(x[2:-1], data_to_plot[timeStep][1:-2], 'bo:', 
                label='$t$ = {0}$\Delta$t={1}'.format(timeStep,round(float(times[timeStep,1]),3)))
    elif timeStep == 38:#len(data_to_plot)-1:
        plt.plot(x[2:-1],data_to_plot[timeStep][1:-2], 'go:',
                label='$t$ = {0}$\Delta$t={1}'.format(timeStep, round(float(times[timeStep,1]),1)))          
xl = xd + ul*T
xr = xd + ur*T

def exact(x,t):
    if x <= xl:
        return ul   
    elif xl < x and x < xr:
        return (x-xd)/t
    elif x >= xr:
        return ur

def ic3(x):
    if x <= 0.3:
        return -1
    elif x <= 0.6:
        return 2
    else:
        return -3

def ic2(x):
    if x < 0.5:
        return 1
    else:
        return -1

ics = [ic3(i) for i in x]
#ics = [np.sin(2*np.pi*i) for i in x]
#ics = [0.5*np.sin(2*np.pi*i) + 1.5 for i in x]
#print("ur = {0}, ul = {1}, xr = {2}, xl = {3}".format(ur,ul, xr, xl))

plt.plot(x[2:-1], ics[2:-1], 'k-', label='initial condition')
plt.tick_params(axis='both', top=False, left=False)
plt.xlabel('x')
plt.ylabel('U')
plt.legend()
plt.title('{2} - my IC Left - N={0}, C={1}'.format(N, C, method))
plt.axis([0.0, 1.1, -3.1, 2.1])
plt.show()
