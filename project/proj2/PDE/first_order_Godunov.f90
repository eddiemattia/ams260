module first_order_Godunov

contains
subroutine FOG(U, nrows, dx, dt)
 
REAL(16), INTENT(IN OUT) :: U(nrows)
INTEGER, INTENT(IN) :: nrows
REAL(16), INTENT(IN) :: dx, dt
INTEGER :: iter
REAL(16) :: F_plus, F_minus, s
REAL(16) :: Utmp(nrows)

Utmp(1) = U(1)
Utmp(nrows) = U(nrows)
DO iter=2,nrows-1
        IF (U(iter) .GE. U(iter+1)) THEN
                ! f(u) = u^2/2
                s = 0.5*(U(iter) + U(iter+1))
                IF (s .GE. 0.0) THEN
                        F_plus = (U(iter)**2)/2
                ELSE
                        F_plus = (U(iter+1)**2)/2
                END IF
        ELSE
                IF (U(iter) .GE. 0.0) THEN
                        F_plus = (U(iter)**2)/2 
                ELSE IF ((U(iter) .LT. 0.0) .AND. (U(iter+1) .GT. 0.0)) THEN
                        F_plus = 0.0
                ELSE IF (U(iter+1) .LE. 0.0) THEN
                        F_plus = (U(iter+1)**2)/2
                END IF
        END IF
        IF (U(iter-1) .GE. U(iter)) THEN
                ! f(u) = u^2/2
                s = 0.5*(U(iter-1) + U(iter))
                IF (s .GE. 0.0) THEN
                        F_minus = (U(iter-1)**2)/2
                ELSE
                        F_minus = (U(iter)**2)/2
                END IF
        ELSE
                IF (U(iter-1) .GE. 0.0) THEN
                        F_minus = (U(iter-1)**2)/2 
                ELSE IF ((U(iter-1) .LT. 0.0) .AND. (U(iter) .GT. 0.0)) THEN
                        F_minus = 0.0
                ELSE IF (U(iter) .LE. 0.0) THEN
                        F_minus = (U(iter)**2)/2
                END IF
        END IF
        Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
END DO
U = Utmp
end subroutine FOG
end module first_order_Godunov
