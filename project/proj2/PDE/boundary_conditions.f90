module boundary_conditions

contains

subroutine boundaryConditions(U, nrows, outflow)

REAL(16), INTENT(IN OUT) :: U(nrows)
INTEGER, INTENT(IN) :: nrows, outflow
INTEGER :: STATUS=0 
! outflow is boolean flag
! alternative is periodic boundary conditions
! all ICs implement outflow except those in
! Eq 7.48 of the lecture notes
IF (outflow .EQ. 1) THEN
      ! outflow condition
      U(1) = U(3)
      U(2) = U(3)
      U(nrows-1) = U(nrows-2)
      U(nrows) = U(nrows-2)
ELSE IF (outflow .EQ. 0) THEN
      ! periodic condition
      U(1) = U(nrows-3)
      U(2) = U(nrows-2)
      U(nrows-1) = U(3)
      U(nrows) = U(4)
ELSE
      write(*,*) "Outflow flag must be 0 or 1, please check advect.f90"
      call EXIT(STATUS)
END IF
end subroutine
end module boundary_conditions
