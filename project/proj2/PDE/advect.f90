program advect
      
      use grid_init
      use advect_init 
      use advect_update
      use cfl_conditions
      use boundary_conditions
      use write_data

        REAL(16) :: dx, dt, C, xa, xb, timeElapsed
        INTEGER :: nrows, N, t, M, write_every, method, icSelection, outflow
        REAL(16), ALLOCATABLE :: U(:), grid(:)
        print *, "MAIN ADVECT.f90 DRIVER"
        open(unit=15, file='advect.init')
        ! advect.init contains one entry per line of:
                ! N, xa, xb, ... TODO
        read(15, *)  N
        !read(15, *) xa
        !read(15, *) xb
        read(15, *) icSelection
        read(15, *) method
        read(15, *) C
        read(15, *) M ! num time steps
        xa=0
        xb=1
        dx = (xb-xa) / N        
        nrows = N + 4
        ALLOCATE( U(nrows))
        ALLOCATE( grid(nrows))
        call gridInit(grid, nrows, xa, dx)
        call advectInit(U, grid, nrows, icSelection)
        IF ((icSelection .EQ. 11) .OR. (icSelection .EQ. 12)) THEN
               outflow=0
        ELSE
               outflow=1
        END IF 
        call cflConditions(dx,dt,C,method)
        print '(F5.3, A3, F5.3, A3, I3, A3, F3.2)',dx," | ", dt, " | ", N, " | ", C
        
        write_every = 1
        timeElapsed = 0.0

        call writeData(U, nrows, method, 0, 0.0, timeElapsed)
        DO t=1,M
                call cflConditions(dx,dt,C, method)
                call boundaryConditions(U, nrows, outflow)
                call advectUpdate(U, nrows, dx, dt, C,  method)
                timeElapsed = timeElapsed + dt
                IF (MOD(t, write_every) .EQ. 0) THEN
                        call writeData(U, nrows, method, t, dt, timeElapsed)
                END IF
        END DO
end program advect
