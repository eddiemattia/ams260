module write_data
contains
subroutine writeData(U, nrows, method, timeN, dt_last, timeElapsed)
REAL(16), INTENT(IN OUT) :: U(nrows)
INTEGER, INTENT(IN) :: timeN, nrows, method
REAL(16), INTENT(IN) :: dt_last, timeElapsed
CHARACTER(len=12) :: timeString
CHARACTER(len=1024) :: filename, fileTmp
INTEGER :: STATUS=0
LOGICAL :: exist
! method: 1-7
      ! 1 = FOG
      ! 2-7 = PLM with slope limiters in order of assignment prompt
filename="data/output_"
IF (method .EQ. 1) THEN
        ! first-order Godunov
        filename = trim(filename)//"FOG_"
ELSE IF (method .EQ. 2) THEN 
        filename = trim(filename)//"PLM_upwind_"
ELSE IF (method .EQ. 3) THEN 
        filename = trim(filename)//"PLM_downwind_"
ELSE IF (method .EQ. 4) THEN 
        filename = trim(filename)//"PLM_centered_"
ELSE IF (method .EQ. 5) THEN
        filename = trim(filename)//"PLM_minmod_"
ELSE IF (method .EQ. 6) THEN
        filename = trim(filename)//"PLM_mc_"
ELSE IF (method .EQ. 7) THEN
        filename = trim(filename)//"PLM_vanLeers_"
ELSE
        write(*,*) "Please change the method in advect.f90 to [1,7]"
        call EXIT(STATUS)
END IF

write(timeString, '(i12)') timeN
fileTmp = filename
filename = trim(fileTmp)//"time"//TRIM(ADJUSTL(timestring))//".dat"
OPEN(unit=13, file=filename, form='formatted', status='replace', action='write')
WRITE(13, *) method
DO iter=1,nrows
        WRITE(13, *) U(iter)
END DO
close(13)
filename = trim(fileTmp)//"timeDiscretization.dat"

INQUIRE(file=filename, exist=exist)

IF (exist .AND. (timeN .NE. 0)) THEN
        OPEN(unit=14, file=filename, status="old", position="append", action="write")
        WRITE(14, '(F12.6, A1, F12.6)') dt_last, " ", timeElapsed
!ELSE IF (exist) THEN
        
ELSE  
        OPEN(unit=14, file=filename, form='formatted', status="replace", action="write")
        WRITE(14, '(F12.6, A1, F12.6)') dt_last, " ", timeElapsed
END IF
close(14)

end subroutine writeData
end module write_data
