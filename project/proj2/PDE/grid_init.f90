module grid_init

contains

subroutine gridInit(grid, nrows, xa, dx)

REAL(16), INTENT(IN OUT) :: grid(nrows)
INTEGER, INTENT(IN) :: nrows
REAL(16), INTENT(IN) :: xa, dx

INTEGER :: iter

print *, "GRID INIT SUBROUTINE"
grid(1) = xa - 1.5*dx
DO iter = 2, nrows 
        grid(iter) = grid(iter - 1) + dx
END DO

end subroutine gridInit

end module grid_init
