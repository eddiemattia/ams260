module advect_init

contains
subroutine advectInit(U, grid, nrows, icSelection)

REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: grid(nrows)
INTEGER, INTENT(IN) :: icSelection
INTEGER :: STATUS=0, nrows, iter
REAL(16), PARAMETER :: PI_16 = 4*ATAN(1.0)

print*,  "IC = ", icSelection

IF (icSelection .EQ. 1) THEN
        ! nonlinear right-going shock (Eq. 7.38)
        DO iter=1,nrows
                IF (grid(iter) .GE. 0.5) THEN
                        U(iter) = 2.0
                ELSE    
                        U(iter) = -1.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 2) THEN
        ! nonlinear standing shock (Eq. 7.39)
        DO iter=1,nrows
                IF (grid(iter) .GE. 0.5) THEN
                        U(iter) = 1.0
                ELSE
                        U(iter)=-1.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 3) THEN
        ! nonlinear left-going shock (Eq. 7.40)
        DO iter=1,nrows
                IF (grid(iter) .GE. 0.5) THEN
                        U(iter ) = 1.0
                ELSE
                        U(iter) = -2.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 4) THEN
        ! nonlinear rareifaction (Eq. 7.41)
        DO iter=1,nrows
                IF (grid(iter) .GE. 0.5) THEN
                        U(iter) = -1.0
                ELSE
                        U(iter)=1.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 5) THEN
        ! nonlinear sine wave evolving to shock (Eq. 7.42)
        DO iter=1,nrows
                U(iter) = SIN(2 * PI_16 * grid(iter))
        END DO
ELSE IF (icSelection .EQ. 6) THEN
        ! nonlinear moving shock and rareifaction (Eq. 7.43)
        DO iter=1,nrows
               IF (grid(iter) .LE. 0.3) THEN
                       U(iter) = 2.0
               ELSE IF ((grid(iter) .LE. 0.6) .AND. grid(iter) .GT. 0.3) THEN
                       U(iter) = -1.0
               ELSE
                       U(iter) = 3.0
               END IF 
        END DO
ELSE IF (icSelection .EQ. 7) THEN
        ! nonlinear rareifaction and stationary shock (Eq. 7.44)
        DO iter=1,nrows
                IF (grid(iter) .LE. 0.3) THEN
                        U(iter) = -1.0
                ELSE IF ((grid(iter) .LE. 0.6) .AND. (grid(iter) .GT. 0.3)) THEN
                        U(iter) = 2.0
                ELSE
                        U(iter) = -2.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 8) THEN
        ! nonlinear right-going shocks evolving into one 
        ! right-going shock (Eq. 7.45)
        DO iter=1,nrows
                IF (grid(iter) .LE. 0.3) THEN
                        U(iter) = 4.0
                ELSE IF ((grid(iter) .LE. 0.6) .AND. (grid(iter) .GT. 0.3)) THEN
                        U(iter) = 2.0
                ELSE
                        U(iter) = -1.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 9) THEN
        ! nonlinear two oppositely-moving shocks evolving into 
        ! one left-going shock (Eq. 7.46)
        DO iter=1,nrows
                IF (grid(iter) .LE. 0.3) THEN
                        U(iter) = 4.0
                ELSE IF ((grid(iter) .LE. 0.6) .AND. (grid(iter) .GT. 0.3)) THEN
                        U(iter) = 0.0
                ELSE
                        U(iter) = -6.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 10) THEN
        ! nonlinear two oppositely-moving shocks evolving into
        ! one standing shock (Eq. 7.47)
        DO iter=1,nrows
                IF (grid(iter) .LE. 0.3) THEN
                        U(iter) = 4.0
                ELSE IF ((grid(iter) .LE. 0.6) .AND. (grid(iter) .GT. 0.3)) THEN
                        U(iter) = 0.0
                ELSE
                        U(iter) = -4.0
                END IF
        END DO
ELSE IF (icSelection .EQ. 11) THEN
        DO iter=1,nrows
                U(iter) = 0.5*SIN(2 * PI_16 * grid(iter)) + 1.5
        END DO
ELSE IF (icSelection .EQ. 12) THEN
        ! TO COMPLETE PART C OF PROJECT ASSIGNMENT
        DO iter=1,nrows
                IF (grid(iter) .LE. 0.3) THEN
                        U(iter) = -1.0
                ELSE IF ((grid(iter) .LE. 0.6) .AND. (grid(iter) .GT. 0.3)) THEN
                        U(iter) = 2.0
                ELSE 
                        U(iter) = -3.0
                END IF
        END DO
ELSE
        write (*,*) "Please choose a valid initial condition (1-11) in advect.init"
        call EXIT(STATUS)
END IF

end subroutine advectInit

end module advect_init
