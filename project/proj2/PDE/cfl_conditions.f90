module cfl_conditions

contains

subroutine cflConditions(dx,dt,C, method)

REAL(16), INTENT(IN) :: dx,C
REAL(16), INTENT(OUT) :: dt
INTEGER, INTENT(IN) :: method

IF (method .EQ. 1) THEN
        ! FOG
        dt = C*dx/2
END IF

end subroutine cflConditions
end module cfl_conditions
