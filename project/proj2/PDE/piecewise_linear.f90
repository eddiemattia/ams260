module piecewise_linear

contains

subroutine upwind(Ui, Uprev, dx, slimiter)
        REAL(16), INTENT(IN) :: Ui, Uprev, dx
        REAL(16), INTENT(OUT) :: slimiter
        slimiter = (Ui-Uprev)/dx
end subroutine upwind
!!!!!!!!!!!!!!!!!!!!!
subroutine downwind(Unext, Ui, dx, slimiter)
        REAL(16), INTENT(IN) :: Unext, Ui, dx
        REAL(16), INTENT(OUT) :: slimiter
        slimiter = (Unext-Ui)/dx
end subroutine downwind
!!!!!!!!!!!!!!!!!!!!!!
subroutine centered(Unext, Uprev, dx, slimiter)
        REAL(16), INTENT(IN) :: Unext, Uprev, dx
        REAL(16), INTENT(OUT) :: slimiter
        slimiter = (Unext-Uprev)/(2*dx)
end subroutine centered
!!!!!!!!!!!!!!!!!!!!!!!
subroutine minmod(a,b,returnVal)
        REAL(16), INTENT(IN) :: a,b
        REAL(16), INTENT(OUT) :: returnVal
        returnVal = 0.5*(SIGN(1.0,a) + SIGN(1.0,b))*MIN(ABS(a), ABS(b))
end subroutine minmod
!!!!!!!!!!!!!!!!!!!!!
subroutine vanLeer(a,b,returnVal)
        REAL(16), INTENT(IN) :: a,b
        REAL(16), INTENT(OUT) :: returnVal
        IF (a*b .LE. 0.0) THEN
                returnVal = 0.0
        ELSE
                returnVal = (2*a*b)/(a+b)
        END IF
end subroutine vanLeer
!!!!!!!!!!!!!!!!!!!!!!
subroutine PLM(U, nrows, dx, dt, C, method)

REAL(16), INTENT(IN OUT) :: U(nrows)
INTEGER, INTENT(IN) :: nrows, method
REAL(16), INTENT(IN) :: dx, C
REAL(16), INTENT(OUT) :: dt
REAL(16) :: slimiter, s, lambdaMax, C_not_cfl, F_plus, F_minus
REAL(16) :: upwindVar, downwindVar, centeredVar, mc1
INTEGER :: iter
REAL(16) :: Utmp(nrows)

Utmp(1) = U(1)
Utmp(nrows) = U(nrows)

IF (method .EQ. 2) THEN 
! upwind slope limiter
        Utmp(1) = U(1)
        Utmp(2) = U(2)
        Utmp(nrows) = U(nrows)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        !print *, "Max wave speed: ", lambdaMax
        dt = C*dx/lambdaMax
        !print *, dt

        DO iter=3,nrows-1
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter+1), U(iter), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call upwind(U(iter+1), U(iter), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                !print *, "F_plus = ", F_plus
                !print *, "F_minus = ", F_minus
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO
ELSE IF (method .EQ. 3) THEN 
! downwind slope limiter
        Utmp(1) = U(1)
        Utmp(nrows) = U(nrows)
        Utmp(nrows-1) = U(nrows-1)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        dt = C*dx/lambdaMax
        DO iter=2,nrows-2
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call downwind(U(iter+1), U(iter), dx, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call downwind(U(iter+2), U(iter+1), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call downwind(U(iter+1), U(iter), dx, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call downwind(U(iter+2), U(iter+1), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call downwind(U(iter), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call downwind(U(iter+1), U(iter), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call downwind(U(iter), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call upwind(U(iter+1), U(iter), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO
ELSE IF (method .EQ. 4) THEN 
! centered slope limiter
        Utmp(1) = U(1)
        Utmp(2) = U(2)
        Utmp(nrows) = U(nrows)
        Utmp(nrows-1) = U(nrows-1)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        dt = C*dx/lambdaMax
        DO iter=3,nrows-2
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call centered(U(iter+1), U(iter-1), dx, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call centered(U(iter+2), U(iter), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call centered(U(iter+1), U(iter-1), dx, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call centered(U(iter+2), U(iter), dx, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call centered(U(iter), U(iter-2), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call centered(U(iter+1), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call centered(U(iter), U(iter-2), dx, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call centered(U(iter+1), U(iter-1), dx, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO
ELSE IF (method .EQ. 5) THEN
!  minmod slope limiter
        Utmp(1) = U(1)
        Utmp(2) = U(2)
        Utmp(nrows) = U(nrows)
        Utmp(nrows-1) = U(nrows)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        dt = C*dx/lambdaMax
        DO iter=3,nrows-2
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)   
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call minmod(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO   
ELSE IF (method .EQ. 6) THEN
! monotonized central differencing slope limiter
        Utmp(1) = U(1)
        Utmp(2) = U(2)
        Utmp(nrows) = U(nrows)
        Utmp(nrows-1) = U(nrows)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        dt = C*dx/lambdaMax
        DO iter=3,nrows-2
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call centered(U(iter+1), U(iter-1), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call centered(U(iter+2), U(iter), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)   
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call centered(U(iter+1), U(iter-1), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call centered(U(iter+2), U(iter), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call centered(U(iter), U(iter-2), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call centered(U(iter+1), U(iter-1), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call centered(U(iter), U(iter-2), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call centered(U(iter+1), U(iter-1), dx, centeredVar)
                                call minmod(2*downwindVar, 2*upwindVar, mc1)
                                call minmod(centeredVar, mc1, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO       
ELSE !IF (method .EQ. 7) THEN
! vanLeers slope limiter
        Utmp(1) = U(1)
        Utmp(2) = U(2)
        Utmp(nrows) = U(nrows)
        Utmp(nrows-1) = U(nrows)
        lambdaMax = 0.0 
        DO iter=1,nrows-1
                s = (U(iter)+U(iter+1))/2
                IF (ABS(s) .GT. lambdaMax) THEN
                        lambdaMax = ABS(s)
                END IF        
        END DO
        dt = C*dx/lambdaMax
        DO iter=3,nrows-2
                s = (U(iter)+U(iter+1))/2
                C_not_cfl = s*dt/dx
                IF (U(iter) .GE. U(iter+1)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)       
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)   
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE 
                        IF (U(iter) .GE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_plus = 0.5*(U(iter) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter) .LT. 0) .AND. (U(iter+1) .GT. 0)) THEN
                                F_plus = 0.0
                        ELSE IF (U(iter+1) .LE. 0) THEN
                                call upwind(U(iter+1), U(iter), dx, upwindVar)
                                call downwind(U(iter+2), U(iter+1), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_plus = 0.5*(U(iter+1) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                s = (U(iter-1)+U(iter))/2
                C_not_cfl = s*dt/dx
                IF (U(iter-1) .GT. U(iter)) THEN
                        IF (s .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                ELSE
                        IF (U(iter-1) .GE. 0) THEN
                                call upwind(U(iter-1), U(iter-2), dx, upwindVar)
                                call downwind(U(iter), U(iter-1), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter-1) + 0.5*dx*slimiter*(1-C_not_cfl))**2
                        ELSE IF ((U(iter-1) .LT. 0) .AND. (U(iter) .GT. 0)) THEN
                                F_minus = 0.0
                        ELSE IF (U(iter) .LE. 0) THEN
                                call upwind(U(iter), U(iter-1), dx, upwindVar)
                                call downwind(U(iter+1), U(iter), dx, downwindVar)
                                call vanLeer(upwindVar, downwindVar, slimiter)
                                F_minus = 0.5*(U(iter) - 0.5*dx*slimiter*(1+C_not_cfl))**2
                        END IF
                END IF
                Utmp(iter) = U(iter) - (dt/dx)*(F_plus - F_minus)
        END DO
END IF
U = Utmp
end subroutine PLM
end module piecewise_linear
