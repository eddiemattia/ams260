module advect_update

use first_order_Godunov
use piecewise_linear

contains
subroutine advectUpdate(U, nrows, dx, dt, C,  method)

REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN OUT) :: C
INTEGER, INTENT(IN OUT) :: nrows, method
REAL(16), INTENT(IN OUT) :: dx, dt
INTEGER :: STATUS=0

IF (method .EQ. 1) THEN
        ! first-order Godunov
        call FOG(U, nrows, dx, dt)
ELSE IF ((method .GE. 2) .AND. (method .LE. 7)) THEN 
        call PLM(U, nrows, dx, dt, C,  method)
ELSE
        write(*,*) "Please change the method in advect.f90 to [1,7]"
        call EXIT(STATUS)
END IF

end subroutine advectUpdate
end module advect_update
