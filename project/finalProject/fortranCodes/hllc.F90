subroutine hllc(vL,vR,Flux)

#include "definition.h"  

  use grid_data
  use sim_data
  use primconsflux, only : prim2flux,prim2cons


  implicit none
  real, dimension(NUMB_VAR), intent(IN) :: vL,vR !prim vars
  real, dimension(NSYS_VAR), intent(OUT):: Flux 

  real, dimension(NSYS_VAR) :: FL,FR,uL,uR,D
  real :: sL,sR,aL,aR

  call prim2flux(vL,FL)
  call prim2flux(vR,FR)
  call prim2cons(vL,uL)
  call prim2cons(vR,uR)

  
  ! left and right sound speed a
  aL = sqrt(vL(GAMC_VAR)*vL(PRES_VAR)/vL(DENS_VAR))
  aR = sqrt(vR(GAMC_VAR)*vR(PRES_VAR)/vR(DENS_VAR))

  ! fastest left and right going velocities
  sL = min(vL(VELX_VAR) - aL,vR(VELX_VAR) - aR)
  sR = max(vL(VELX_VAR) + aL,vR(VELX_VAR) + aR)
  
  ! middle wave (10.70)
  sStar = (vR(PRES_VAR) - vL(PRES_VAR) + vL(DENS_VAR)*vL(VELX_VAR)*(sL - vL(VELX_VAR)) - &
          vR(PRES_VAR)*vR(VELX_VAR)*(sR - vR(VELX_VAR))) / &
          (vL(DENS_VAR)*(sL - vL(VELX_VAR) - vR(DENS_VAR)*(sR - vR(VELX_VAR))))

  D = (/0., 1., sStar/)

  ! numerical flux
  if (sL >= 0.) then
     Flux(DENS_VAR:ENER_VAR) = FL(DENS_VAR:ENER_VAR)
  elseif ( (sL < 0.) .and. (sStar >= 0.) ) then

     Flux(DENS_VAR:ENER_VAR) = (sStar*(sL*uL(DENS_VAR:ENER_VAR) - FL(DENS_VAR:ENER_VAR) &
                                 + sL*()FR(DENS_VAR:ENER_VAR) &
                                 + sR*sL*(uR(DENS_VAR:ENER_VAR) &
                                      -uL(DENS_VAR:ENER_VAR)))/(sR-sL)

  else if ( (sStar < 0.) .and. (sR >= 0.) ) then
     !   pStarL = FL(PRES_VAR
     !   if (sStar >= 0.) then
     !   Flux(DENS_VAR:ENER_VAR) = (sStar*()+sL*())/(sL-sStar)
     !   else 
     !   Flux(DENS_VAR:ENER_VAR) = FR(DENS_VAR:ENER_VAR) + sR*(uStarR - uR)
     !   end if
     !end if
  else
     Flux(DENS_VAR:ENER_VAR) = FR(DENS_VAR:ENER_VAR)
  endif

  return
end subroutine hllc
