subroutine soln_PPM(dt)

#include "definition.h" 

      use grid_data
      use sim_data
      use slopeLimiter
      use eigensystem
      implicit none
      real, intent(IN) :: dt
      integer :: i

      real, dimension(NUMB_WAVE) :: lambda, lambdaRight, lambdaLeft
      real, dimension(NSYS_VAR,NUMB_WAVE) :: reig,reigRight,reigLeft
      real ,dimension(NSYS_VAR,NUMB_WAVE) :: leig,leigRight,leigLeft
      logical :: conservative
      real, dimension(NSYS_VAR) :: vL, vR, vecL, vecR
      integer :: kWaveNum
      real :: lambdaDtDx, arg1, arg2, arg1Left, arg1Right, arg2Left, arg2Right
      real, dimension(NSYS_VAR) :: delVi, delViRight, delViLeft, delL, delR
      real, dimension(NSYS_VAR) :: delLLeft, delLRight, delRLeft, delRRight
      real, dimension(NSYS_VAR) :: C0, C1, C2, sigL1, sigL2, sigR1, sigR2
      real, dimension(NUMB_WAVE) :: delW, delWLeft, delWRight, delC1, delC2
      integer :: nVar

      conservative = .false.

      do i = gr_ibeg-1, gr_iend+1

        ! Step 1: Parabolic Profile (page 157) 

        ! Get necessary data to obtain vL and vR
        call eigenvalues(gr_V(DENS_VAR:GAME_VAR,  i), lambda)
        call eigenvalues(gr_V(DENS_VAR:GAME_VAR,i+1), lambdaRight)
        call eigenvalues(gr_V(DENS_VAR:GAME_VAR,i-1), lambdaLeft)
        call right_eigenvectors(gr_V(DENS_VAR:GAME_VAR,  i),conservative,reig)
        call right_eigenvectors(gr_V(DENS_VAR:GAME_VAR,i+1),conservative,reigRight)
        call right_eigenvectors(gr_V(DENS_VAR:GAME_VAR,i-1),conservative,reigLeft)
        call left_eigenvectors(gr_V(DENS_VAR:GAME_VAR,  i) ,conservative,leig)
        call left_eigenvectors(gr_V(DENS_VAR:GAME_VAR,i+1) ,conservative,leigRight)
        call left_eigenvectors(gr_V(DENS_VAR:GAME_VAR,i-1) ,conservative,leigLeft)
        delL      = gr_V(DENS_VAR:PRES_VAR,  i) - gr_V(DENS_VAR:PRES_VAR,i-1)
        delR      = gr_V(DENS_VAR:PRES_VAR,i+1) - gr_V(DENS_VAR:PRES_VAR,i)
        delLLeft  = gr_V(DENS_VAR:PRES_VAR,i-1) - gr_V(DENS_VAR:PRES_VAR,i-2)
        delLRight = gr_V(DENS_VAR:PRES_VAR,  i) - gr_V(DENS_VAR:PRES_VAR,i-1)
        delRLeft  = gr_V(DENS_VAR:PRES_VAR,i+1) - gr_V(DENS_VAR:PRES_VAR,i)
        delRRight = gr_V(DENS_VAR:PRES_VAR,i+2) - gr_V(DENS_VAR:PRES_VAR,i+1)
        ! TODO remove repeated data, i.e. delRLeft=delR, delLRight=delL

        ! PRIMITIVE LIMITING
        if (.not. sim_charLimiting) then

           ! Slope Limiters directly obtainable by (9.48) on page 158     
           do nVar = DENS_VAR,PRES_VAR
              if (sim_limiter == 'minmod') then
                 call minmod(delL(nVar)     , delR(nVar)     , delVi(nVar))
                 call minmod(delLLeft(nVar) , delRLeft(nVar) , delViLeft(nVar))
                 call minmod(delLRight(nVar), delRRight(nVar), delViRight(nVar))
              else if (sim_limiter == 'vanLeer') then
                 call vanLeer(delL(nVar)     , delR(nVar)     , delVi(nVar))
                 call vanLeer(delLLeft(nVar) , delRLeft(nVar) , delViLeft(nVar))
                 call vanLeer(delLRight(nVar), delRRight(nVar), delViRight(nVar))
              else if (sim_limiter == 'mc') then
                 call mc(delL(nVar)     , delR(nVar)     , delVi(nVar))
                 call mc(delLLeft(nVar) , delRLeft(nVar) , delViLeft(nVar))
                 call mc(delLRight(nVar), delRRight(nVar), delViRight(nVar))
              end if
           end do

        else ! CHARACTERISTIC LIMITING

           ! get slope limiters in characteristic variables space
           do kWaveNum = 1,NUMB_WAVE

              arg1      = dot_product(     leig(DENS_VAR:PRES_VAR,kWaveNum),delL(DENS_VAR:PRES_VAR))
              arg2      = dot_product(     leig(DENS_VAR:PRES_VAR,kWaveNum),delR(DENS_VAR:PRES_VAR))
              arg1Left  = dot_product( leigLeft(DENS_VAR:PRES_VAR,kWaveNum),delLLeft(DENS_VAR:PRES_VAR))
              arg2Left  = dot_product( leigLeft(DENS_VAR:PRES_VAR,kWaveNum),delRLeft(DENS_VAR:PRES_VAR))
              arg1Right = dot_product(leigRight(DENS_VAR:PRES_VAR,kWaveNum),delLRight(DENS_VAR:PRES_VAR))
              arg2Right = dot_product(leigRight(DENS_VAR:PRES_VAR,kWaveNum),delRRight(DENS_VAR:PRES_VAR))
              
              ! (9.50) 
              if (sim_limiter == 'minmod') then
                 call minmod(arg1     , arg2     , delW(kWaveNum))
                 call minmod(arg1Left , arg2Left , delWLeft(kWaveNum))
                 call minmod(arg1Right, arg2Right, delWRight(KWaveNum))
              else if (sim_limiter == 'vanLeer') then
                 call vanLeer(arg1     , arg2     , delW(kWaveNum))
                 call vanLeer(arg1Left , arg2Left , delWLeft(kWaveNum))
                 call vanLeer(arg1Right, arg2Right, delWRight(kWaveNum))
              else if (sim_limiter == 'mc') then
                 call mc(arg1     , arg2     , delW(kWaveNum))
                 call mc(arg1Left , arg2Left , delWLeft(kWaveNum))
                 call mc(arg1Right, arg2Right, delWRight(kWaveNum))
              end if       
           end do
           
           ! project slope limiters back into primitive variable space (9.49)
           do nVar = DENS_VAR,PRES_VAR
              delVi(nVar) = dot_product(reig(nVar,1:NUMB_WAVE),delW(1:NUMB_WAVE))
              delViLeft(nVar) = dot_product(reigLeft(nVar,1:NUMB_WAVE),delWLeft(1:NUMB_WAVE))
              delViRight(nVar) = dot_product(reigRight(nVar,1:NUMB_WAVE),delWRight(1:NUMB_WAVE))
           end do

        end if

        ! (9.47) a0-
        vL = 0.5*(gr_V(DENS_VAR:PRES_VAR,i-1) + gr_V(DENS_VAR:PRES_VAR,i)) - (1/6)*(delVi - delViLeft) 
        ! (9.47) a0+
        vR = 0.5*(gr_V(DENS_VAR:PRES_VAR,i) + gr_V(DENS_VAR:PRES_VAR,i+1)) - (1/6)*(delViRight - delVi)
        
        do nVar = DENS_VAR,PRES_VAR
           if ((vR(nVar) - gr_V(nVar,i))*(gr_V(nVar,i)-vL(nVar)) <= 0) then
                ! (9.51) PPM reduces to FOG 
                vL(nVar) = gr_V(nVar,i)
                vR(nVar) = gr_V(nVar,i)
           else if ( -(vR(nVar)-vL(nVar))**2 > 6*(vR(nVar)-vL(nVar))*(gr_V(nVar,i) - 0.5*(vR(nVar)+vL(nVar)))) then
                ! (9.52)
                vR(nVar) = 3*gr_V(nVar,i) - 2*vL(nVar)
           else if ( (vR(nVar)-vL(nVar))**2 < 6*(vR(nVar)-vL(nVar))*(gr_V(nVar,i) - 0.5*(vR(nVar)+vL(nVar)))) then
                ! (9.53)
                vL(nVaR) = 3*gr_V(nVar,i) - 2*vR(nVar)
           end if
        end do

        ! Step 2: Characteristic Tracing
        !(9.38)
        C2 = (6/gr_dx**2)*(0.5*(vR + vL) - gr_V(DENS_VAR:PRES_VAR, i))
        !(9.37)
        C1 = (1/gr_dx)*(vR - vL)
        !(9.36)
        C0 = gr_V(DENS_VAR:PRES_VAR, i) - (gr_dx**2/12)*C2

        do kWaveNum = 1,NUMB_WAVE
          !(9.57)
          delC1(kWaveNum) = gr_dx*dot_product(leig(DENS_VAR:PRES_VAR,kWaveNum), C1)
          !(9.59) 
          delC2(kWaveNum) = (gr_dx**2)*dot_product(leig(DENS_VAR:PRES_VAR,kWaveNum), C2) 
        end do

        sigL1(DENS_VAR:PRES_VAR) = 0.
        sigL2(DENS_VAR:PRES_VAR) = 0.
        sigR1(DENS_VAR:PRES_VAR) = 0.
        sigR2(DENS_VAR:PRES_VAR) = 0.

        do kWaveNum = 1,NUMB_WAVE
           
           lambdaDtDx = lambda(kWaveNum)*dt/gr_dx

           if (sim_riemann == 'roe') then
              if (lambda(kWaveNum) < 0.) then
                 sigL1(DENS_VAR:PRES_VAR) = sigL1(DENS_VAR:PRES_VAR) + 0.5*(-1.0 - lambdaDtDx)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC1(kWavenum)
                 sigL2(DENS_VAR:PRES_VAR) = sigL2(DENS_VAR:PRES_VAR) + 0.25*(1.0 + 2*lambdaDtDx + (4/3)*lambdaDtDx**2)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC2(kWaveNum)
              else if (lambda(kWaveNum) > 0.) then
                 sigR1(DENS_VAR:PRES_VAR) = sigR1(DENS_VAR:PRES_VAR) + 0.5*(1.0 - lambdaDtDx)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC1(kWavenum)
                 sigR2(DENS_VAR:PRES_VAR) = sigR2(DENS_VAR:PRES_VAR) + 0.25*(1.0 - 2*lambdaDtDx + (4/3)*lambdaDtDx**2)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC2(kWaveNum)
              end if
           else if (sim_riemann == 'hll') then
                 sigL1(DENS_VAR:PRES_VAR) = sigL1(DENS_VAR:PRES_VAR) + 0.5*(-1.0 - lambdaDtDx)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC1(kWavenum)
                 sigL2(DENS_VAR:PRES_VAR) = sigL2(DENS_VAR:PRES_VAR) + 0.25*(1.0 + 2*lambdaDtDx + (4/3)*lambdaDtDx**2)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC2(kWaveNum)
                 sigR1(DENS_VAR:PRES_VAR) = sigR1(DENS_VAR:PRES_VAR) + 0.5*(1.0 - lambdaDtDx)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC1(kWavenum)
                 sigR2(DENS_VAR:PRES_VAR) = sigR2(DENS_VAR:PRES_VAR) + 0.25*(1.0 - 2*lambdaDtDx + (4/3)*lambdaDtDx**2)*reig(DENS_VAR:PRES_VAR,kWaveNum)*delC2(kWaveNum)
           end if 
        end do

        gr_vL(DENS_VAR:NUMB_VAR,i) = gr_V(DENS_VAR:NUMB_VAR,i)
        gr_vR(DENS_VAR:NUMB_VAR,i) = gr_V(DENS_VAR:NUMB_VAR,i)

        gr_vL(DENS_VAR:PRES_VAR,i) = gr_V(DENS_VAR:PRES_VAR,i)+sigL1(DENS_VAR:PRES_VAR)+sigL2(DENS_VAR:PRES_VAR)
        gr_vR(DENS_VAR:PRES_VAR,i) = gr_V(DENS_VAR:PRES_VAR,i)+sigR1(DENS_VAR:PRES_VAR)+sigR2(DENS_VAR:PRES_VAR) 
        
      end do
      return
end subroutine
