program advect
    
    use grid_init
    use advect_init
    use advect_update
    use cfl
    use bc
    use write_data

    REAL(16) :: dx, dt, a, C, xa, xb
    INTEGER :: nrows, N, M, t, continuous, write_every
    REAL(16), ALLOCATABLE :: U(:), grid(:), U_prev(:)

    CHARACTER (len=1) :: method

!!!! PARAMETERS !!!!
! these should match what is the visualization.py file !
    N = 200
    nrows = N+4
    xa = -1.0
    xb = 1.0
    dx = (xb - xa) / N
    C = 0.9 ! Courant number for CFL condition
    a = 1.0 ! constant advection velocity
    M = 890
    write_every = FLOOR(M/5.0)
!!!!!!!!!!!!!!!!!!!!!!

    ALLOCATE( U(nrows) )
    ALLOCATE( grid(nrows) )
    ALLOCATE( U_prev(nrows) )

    !!!!!!!!!!!!! SET (2) PARAMETERS !!!!!!!!!!!!!!
    ! Is the model continuous? This determines: 
        ! 1. initialization of U 
                ! (1 -> continuous, 2 -> Jiang and Shu ICs
                    ! else -> discontinouous)
        ! 2. How boundary conditions are set
                ! (1 || 2) -> periodic condition, 
                    ! else -> outflow condition)

    continuous = 2
    method = "1"

    ! What method do you want to use to approximate with? 
    ! CHANGE TO CORRESPOND DICT ENTRY FOR METHOD YOU WANT TO USE
    ! "1":"upwind", "2":"downwind", 
    ! "3":"centered", "4":"Leapfrog", 
    ! "5":"Lax-Friedrichs", "6":"Lax-Wendroff", 
    ! "7":"Beam-Warming", "8":"Fromm"
    !!!!!!!!!!!!! END PARAMETERS !!!!!!!!!!!!!!

    ! set up grid configuration
    call grid_init_routine(grid, nrows, xa, dx) 

    ! set up init cond for advection
    call advect_init_routine(U, grid, nrows, continuous) 

    DO t = 1,M+1

        ! apply boundary conditions, periodic|outflow       
        call bc_routine(U, nrows, continuous)  

        ! call the CFL condition for advection
        call cfl_routine(dx, dt, a, C)   

        ! update advection equation
        call advect_update_routine(U, nrows, dx, dt, a, U_prev, method) 

        IF ((t .EQ. 1) .OR. (MOD(t, write_every) .EQ. 0)) THEN
            ! write results to standard ASCII file
            call write_data_routine(U, nrows, method, t)
            ! ***Go to write_data.f90 to change output file***
        END IF
    END DO       

end program advect








