module downwind

contains

!###################################
subroutine downwind_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)

! implementation of the downwind method
    ! AKA Forward time forward space (FTFS)

! print *, "DOWNWIND SUBROUTINE"

DO iter = 1,nrows-1
    U_next(iter) = U(iter) - (a*dt/dx) * (U(iter+1) - U(iter))
END DO

U_next(nrows) = U(nrows)

U = U_next

end subroutine downwind_routine
!###################################
end module downwind