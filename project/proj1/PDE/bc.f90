module bc

contains

!###################################
subroutine bc_routine(U, nrows, continuous)

REAL(16), INTENT(IN OUT) :: U(nrows)
INTEGER, INTENT (IN) :: nrows, continuous

! INTEGER :: iter !!!! TEMPORARY !!!!

! this applies boundary condition for advection

! print *, "BC SUBROUTINE"

! Continuous - Periodic Condition
IF ((continuous .EQ. 1) .OR. (continuous .EQ. 2)) THEN 
    U(1) = U(nrows - 3)
    U(2) = U(nrows - 2)
    U(nrows - 1) = U(3)
    U(nrows) = U(4)

! Riemann Problem - Outflow Condition   
ELSE 
    U(1) = U(2)
    U(2) = U(2)
    U(nrows - 1) = U(nrows - 2)
    U(nrows) = U(nrows - 2)
END IF

end subroutine bc_routine
!###################################
end module bc