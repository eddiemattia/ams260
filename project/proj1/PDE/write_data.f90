module write_data

contains

!###################################
subroutine write_data_routine(U, nrows, method, timeN) 

REAL(16), INTENT (IN OUT) :: U(nrows)
INTEGER, INTENT (IN) :: timeN
INTEGER, INTENT (IN) :: nrows
CHARACTER(len=1), INTENT(IN) :: method
CHARACTER(len=12) :: timeString
INTEGER :: iter

CHARACTER(len=1024) :: filename

filename = "data/output_"

!print *, "WRITE DATA SUBROUTINE"

IF (method .EQ. "1") THEN
    filename = trim(filename)//"upwind_"
ELSE IF (method .EQ. "2") THEN 
    filename = trim(filename)//"downwind_"
ELSE IF (method .EQ. "3") THEN 
    filename = trim(filename)//"centered_"
ELSE IF (method .EQ. "4") THEN 
    filename = trim(filename)//"leapfrog_"
ELSE IF (method .EQ. "5") THEN 
    filename = trim(filename)//"laxFriedrichs_"
ELSE IF (method .EQ. "6") THEN 
    filename = trim(filename)//"laxWendroff_"
ELSE IF (method .EQ. "7") THEN 
    filename = trim(filename)//"beamWarming_"
ELSE IF (method .EQ. "8") THEN 
    filename = trim(filename)//"fromm_"
END IF

write(timestring, '(i12)') timeN

!filename = trim(filename)//"time"//TRIM(ADJUSTL(timestring))//".dat"
filename = trim(filename)//"time"//TRIM(ADJUSTL(timestring))//".txt"


print *, trim(filename)

OPEN(UNIT=2, FILE=filename, FORM='FORMATTED',STATUS='REPLACE', ACTION='WRITE')
!OPEN(UNIT=2, FILE=filename, FORM='UNFORMATTED',STATUS='REPLACE', ACTION='WRITE')

WRITE(2, *) method

! This writes data out to ASCII file 
DO iter = 1,nrows 
    WRITE(2, *) U(iter)
END DO

end subroutine write_data_routine
!###################################
end module write_data