module advect_update

use upwind
use downwind
use centered
use leapfrog
use lax_friedrichs
use lax_wendroff
use beam_warming
use fromm

contains

!###################################
subroutine advect_update_routine(U, nrows, dx, dt, a, U_prev, method)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows), U_prev(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows
CHARACTER(len=1), INTENT(IN) :: method

!   This sets up the grid configuration

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!print *, "ADVECT UPDATE SUBROUTINE"
!print *,""
!print *, "please type a number to choose an update method out of: "
!print *, "  (1) Upwind"
!print *, "  (2) Downwind"
!print *, "  (3) Centered"
!print *, "  (4) Leapfrog"
!print *, "  (5) Lax-Friedrichs"
!print *, "  (6) Lax-Wendroff"
!print *, "  (7) Beam-warming"
!print *, "  (8) Fromm"
!read(*,*) c
!print*,""
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

IF (method .Eq. "1") THEN
    call upwind_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "2") THEN
    call downwind_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "3") THEN
    call centered_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "4") THEN
    call leapfrog_routine(U, nrows, dx, dt, a, U_prev)

ELSE IF (method .Eq. "5") THEN
    call lax_friedrichs_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "6") THEN
    call lax_wendroff_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "7") THEN
    call beam_warming_routine(U, nrows, dx, dt, a)

ELSE IF (method .Eq. "8") THEN
    call fromm_routine(U, nrows, dx, dt, a)

END IF

end subroutine advect_update_routine
!###################################
end module advect_update