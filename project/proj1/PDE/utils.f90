module utils

contains

!###################################
subroutine G(x, beta, z, out)

REAL(16), INTENT(IN) :: x, beta, z
REAL(16), INTENT(IN OUT) :: out

out = EXP( -beta*(x - z)**2 )
 
end subroutine G
!###################################
subroutine F(x, alpha, a, out)

REAL(16), INTENT(IN) :: x, alpha, a
REAL(16), INTENT(IN OUT) :: out

out = SQRT( MAX(1 - (alpha**2) * (x-a)**2 , 0.0 ) ) 
 
end subroutine F
!###################################
end module utils
