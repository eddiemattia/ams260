module beam_warming

contains

!###################################
subroutine beam_warming_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)
INTEGER :: iter

! implementation of the beam-warming method

! print *, "BEAM-WARMING SUBROUTINE"

U_next(1) = U(1)
U_next(2) = U(2)

DO iter = 3,nrows
    U_next(iter) = U(iter) 
    U_next(iter) = U_next(iter) - ((a*dt) / (2.0*dx)) * (3.0*U(iter) - 4.0*U(iter - 1) + U(iter-2)) 
    U_next(iter) = U_next(iter) + (0.5 * ((a*dt)/dx)**2) * (U(iter) - 2.0 * U(iter - 1) + U(iter - 2))
END DO

U = U_next

end subroutine beam_warming_routine
!###################################
end module beam_warming