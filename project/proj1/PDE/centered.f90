module centered

contains

!###################################
subroutine centered_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)
INTEGER :: iter

! implementation of the centered method
    ! AKA Forward time centered space

! print *, "CENTERED SUBROUTINE"

U_next(1) = U(1)
U_next(nrows) = U(nrows)

DO iter = 2,nrows-1
    U_next(iter) = U(iter) - ((a*dt) / (2.0*dx)) * (U(iter+1) - U(iter-1))
END DO

U = U_next

end subroutine centered_routine
!###################################
end module centered