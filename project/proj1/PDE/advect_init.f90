module advect_init

use utils

contains

!###################################
subroutine advect_init_routine(U, grid, nrows, continuous) 

REAL(16), INTENT (IN OUT) :: U(nrows)
REAL(16), INTENT (IN) :: grid(nrows)
INTEGER, INTENT (IN) :: nrows
INTEGER, INTENT (IN) :: continuous

REAL(16), PARAMETER :: PI = 3.141592653589793 !4 * ATAN(1.0_16)

REAL(16) :: x, tmp1, tmp2, tmp3

! This sets up the initial condition for advection

! print *, "ADVECT INIT SUBROUTINE"

IF (continuous .EQ. 1) THEN
    ! CONTINUOUS CASE
    DO iter = 1,nrows
        U(iter) = SIN(2*PI * grid(iter))
        !print *, iter, ": ", U(iter)
    END DO

ELSE IF (continuous .EQ. 2) THEN
    ! Jiang and Shu (JCP, 126, pp. 202-228 (1996))
    ! see paper for discussion of these IC's

    ! CONSTANTS
    delta = 0.005
    z = -0.7
    beta = LOG(2.0)/(36.0 * delta**2)
    a = 0.5
    alpha = 10

    print *, "IN THE FUNC"

    DO iter = 1,nrows
        x = grid(iter)
        IF ((x .GE. -0.8) .AND. (x .LE. -0.6)) THEN
            call G(x, beta, z - delta, tmp1)
            call G(x, beta, z + delta, tmp2)
            call G(x, beta, z, tmp3)
            U(iter) = (1.0/6.0) * (tmp1 + tmp2 + 4*tmp3)
        ELSE IF ((x .GE. -0.4) .AND. (x .LE. -0.2)) THEN
            U(iter) = 1
        ELSE IF ((x .GE. 0.0) .AND. (x .LE. 0.2)) THEN
            U(iter) = 1 - ABS(10 * (x - 0.1))
        ELSE IF ((x .GE. 0.4) .AND. (x .LE. 0.6)) THEN
            call F(x, alpha, a - delta, tmp1)
            call F(x, alpha, a + delta, tmp2)
            call F(x, alpha, a, tmp3)
            U(iter) = (1.0/6.0) * (tmp1 + tmp2 + 4*tmp3)
        ELSE
            U(iter) = 0
        END IF
    END DO

ELSE
    ! DISCONTINUOUS CASE
    DO iter = 1,nrows
        IF (grid(iter) .LT. 0.5) THEN
            U(iter) = 1.0
        ELSE
            U(iter) = -1.0
        END IF
        !print *, iter, ": ", U(iter)
    END DO
END IF

end subroutine advect_init_routine
!###################################
end module advect_init