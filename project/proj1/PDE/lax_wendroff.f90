module lax_wendroff

contains

!###################################
subroutine lax_wendroff_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)
INTEGER :: iter

! implementation of the lax wendroff (LW) method

! print *, "lax wendroff SUBROUTINE"

U_next(1) = U(1)
U_next(nrows) = U(nrows)

DO iter = 2,nrows-1
    U_next(iter) = U(iter)
    U_next(iter) = U_next(iter) - ((a*dt)/(2.0*dx)) * (U(iter + 1) - U(iter - 1))
    U_next(iter) = U_next(iter) + 0.5 * (((a*dt) / dx)**2) * (U(iter + 1) - 2.0 * U(iter) + U(iter - 1))
END DO

U = U_next

end subroutine lax_wendroff_routine
!###################################
end module lax_wendroff