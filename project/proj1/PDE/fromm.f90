module fromm

contains

!###################################
subroutine fromm_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)
INTEGER :: iter

! implementation of the Fromm method

! print *, "FROMM SUBROUTINE"

U_next(1) = U(1)
U_next(2) = U(2)
U_next(nrows) = U(nrows)

DO iter = 3,nrows-1
    U_next(iter) = U(iter)
    U_next(iter) = U_next(iter) - ((a*dt) / dx) * (U(iter) - U(iter - 1)) 
    U_next(iter) = U_next(iter) - .25 * ((a*dt) / dx) * (1 - ((a*dt)/dx)) * (U(iter + 1) - U(iter)) 
    U_next(iter) = U_next(iter) + .25 * ((a*dt)/dx) * (1 - ((a*dt)/dx)) * (U(iter - 1) - U(iter - 2))
END DO

U = U_next

end subroutine fromm_routine
!###################################
end module fromm