module leapfrog

contains

!###################################
subroutine leapfrog_routine(U, nrows, dx, dt, a, U_prev)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows), U_prev(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL :: U_next(nrows)
INTEGER :: iter

! implementation of the leapfrog method

! print *, "LEAPFROG SUBROUTINE"

U_next(1) = U(1)
U_next(nrows) = U(nrows)

DO iter = 2,nrows-1
    U_next(iter) = U_prev(iter) - ((a*dt) / dx) * (U(iter+1) - U(iter-1))
END DO

U_prev = U
U = U_next

end subroutine leapfrog_routine
!###################################
end module leapfrog