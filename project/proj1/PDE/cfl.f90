module cfl

contains

!###################################
subroutine cfl_routine(dx, dt, a, C)

REAL(16), INTENT(IN) :: dx, a, C
REAL(16), INTENT(OUT) :: dt

! this calls CFL condition for advection

! print *, "CFL SUBROUTINE"

dt = C * (dx / ABS(a))

end subroutine cfl_routine
!###################################
end module cfl