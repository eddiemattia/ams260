module upwind

contains

!###################################
subroutine upwind_routine(U, nrows, dx, dt, a)

! ARGUMENTS
REAL(16), INTENT(IN OUT) :: U(nrows)
REAL(16), INTENT(IN) :: dx, dt, a
INTENT(IN) :: nrows

REAL(16) :: U_next(nrows)
INTEGER :: iter

! implementation of the upwind method
    ! AKA Forward time backward space (FTBS)

! print *, "UPWIND SUBROUTINE"

U_next(1) = U(1)

DO iter = 2,nrows
    U_next(iter) = U(iter) - (a*dt/dx) * (U(iter) - U(iter-1))
END DO

U = U_next

end subroutine upwind_routine
!###################################
end module upwind