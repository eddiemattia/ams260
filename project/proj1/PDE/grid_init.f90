module grid_init

contains

!###################################
subroutine grid_init_routine(grid, nrows, xa, dx)

REAL(16), INTENT(IN OUT) :: grid(nrows)
INTEGER, INTENT(IN) :: nrows
REAL(16), INTENT(IN) :: xa, dx

! INTEGER :: iter

! This sets up the grid configuration

grid(1) = xa - 1.5*dx

! print *, "GRID INIT SUBROUTINE"

DO iter = 2, nrows
    grid(iter) = grid(iter - 1) + dx
    ! print *, iter, ": ", grid(iter)
END DO
 
end subroutine grid_init_routine
!###################################
end module grid_init
