\documentclass[a4paper,11pt]{article}
\usepackage[left=2cm, right=2cm, top=2cm]{geometry}
\usepackage[english]{babel}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{float}
\let\biconditional\leftrightarrow

\begin{document}
\title{\Large{\textbf{PROJECT 1 | AMS260}}}
\author{Eddie Mattia}
\date{Due: April 30, 2019}

\maketitle

\setcounter{page}{1}

\section*{ABSTRACT}
This report details a project done for AMS260 - Computational Fluid Dynamics. The numerical codes are written in Fortran 90 and python is used for visualization. The objective is to design a program that is able to initialize and progress an approximation of a partial differential equation through time. Currently, the codes are set up to perform linear scalar advection by invoking one of the following models: upwind method, downwind method, centered method, leapfrog method, lax-friedrichs method, lax-wendroff method, beam-warming method, and fromm's method. These methods are defined and the results of each for propagating a continuous wave and for propagating a discontinuous shock are evaluated. The results show that 

\section*{DESCRIPTION OF METHODS}
We are interested in approximating the scalar linear advection PDE:

$$u_t + au_x = 0, 0 \leq x \leq 1$$
where a is the constant advection velocity. 
\newline\newline
To do this, we must first discretize our system. We do this by creating a uniform grid. Note that the grid need not be uniform, but that parameter was held constant for this version of the project. The grid is constructed by dividing the range of the spatial domain by the spatial resolution, i.e. $\Delta x = \frac{x_b - x_a}{N}$, where $x_a$, $x_b$, and $N$ are the lower spatial bound, upper spatial bound, and spatial resolution, respectively. Importantly, we add two ghost cells on either side of the grid so that we are able to impose boundary conditions.
\newline\newline
Once the spatial grid is constructed we are able to initialize the numerical approximation for $u$ at time step $t=0$ by invoking initial conditions given. In this code we use $u(x, t=0) = sin(2\pi x)$ to analyze a continuous flow, and $u(x,t=0) = \begin{cases} 1\ \text{for $x < 0.5$} \\ -1\ \text{for $x > 0.5$} \end{cases}$ as a discontinuous initial profile to analyze the flow of a shock. 
\newline\newline
For the continuous sine wave advection, we impose periodic boundary conditions:
$$U_0^n = U_{N}^n$$ 
$$U_1^n = U_{N+1}^n$$ 
$$U_{N+2}^n = U_2^n$$ 
$$U_{N+3}^n = U_3^n$$ 
\newline
For the discontinuous advection, we impose outflow conditions at the boundary:
$$U_0^n = U_2^n$$
$$U_1^n = U_2^n$$ 
$$U_{N+2}^n = U_{N+1}^n$$
$$U_{N+3}^n = U_{N+1}^n$$ 
\newline
In the project use eight different methods to march our approximation $U_i^n \approx u(x_i, t^n)$ forward in time, and the results are in the next section. The update methods are defined as follows:
\newline

- Upwind: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{\Delta x}(u_i^n - u_{i-1}^n)$
\newline

- Downwind: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{\Delta x}(u_{i+1}^n - u_{i}^n)$
\newline

- Centered: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{2 \Delta x}(u_{i+1}^n - u_{i-1}^n)$
\newline

- Leapfrog: $u_i^{n+1} = u_i^{n-1} - \frac{a\Delta t}{\Delta x}(u_{i+1}^n - u_{i-1}^n)$
\newline

- Lax-Friedrichs: $u_i^{n+1} = u_i^n + \frac{1}{2}(u_{i+1}^n + u_{i-1}^n) - \frac{a\Delta t}{2 \Delta x}(u_{i+1}^n - u_{i-1}^n)$
\newline

- Lax-Wendroff: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{2 \Delta x}(u_{i+1}^n - u_{i-1}^n) + \frac{1}{2}(\frac{a\Delta t}{\Delta x})^2(u_{i+1}^n - 2u_i^n + u_{i-1}^n)$
\newline

- Beam-Warming: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{2\Delta x}(3u_i^n - 4u_{i-1}^n + u_{i-1}^n) + \frac{1}{2}(\frac{a\Delta t}{\Delta x})^2(u_i^n - 2u_{i-1}^n + u_{i-2}^n)$
\newline

- Fromm: $u_i^{n+1} = u_i^n - \frac{a\Delta t}{\Delta x}(u_i^n - u_{i-1}^n) - \frac{a\Delta t}{4\Delta x}(1-\frac{a\Delta t}{\Delta x})(u_{i+1}^n - u_i^n) + \frac{a\Delta t}{\Delta x}(1 - \frac{a\Delta t}{\Delta x})(u_{i-1}^n - u_{i-2}^n)$
\newline\newline
These methods provide the instructions for how we move the initial distribution of values of $U$ at the grid points as time move to time $t = tMax$. Another important aspect of the simulation is the imposition of the CFL condition. The CFL condition is $0 < \Delta t \leq C \frac{\Delta x}{|a|}$ where $0 < C \leq 1$ (chosen to be $0.9$). It places conditions on $\Delta t$, and is required for a numerical algorithm to be stable. Note that it does not imply the algorithm will converge, but is merely a necessary condition for convergence. 
\newline

\section*{RESULTS}
The plots on the following page illustrate the accuracy of the approximation for various methods. The exact solution to the continuous version (left column) and discontinuous version (right column) are shown for five methods. The time $t_{max}$ to complete one cycle for the sine wave advection was found to be $t_{max} = 1.0125$. The time for the discontinuous shock to move from it's initial position at $x=0.5$ to $x=0.8$ was $t_{max} = 0.3094$.
\newpage

\begin{figure}[H]
\centering
\begin{tabular}{cc}

\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_discontinuous.png} \\
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxFriedrichs_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxFriedrichs_discontinuous.png} \\
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxWendroff_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxWendroff_discontinuous.png} \\ 
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{beamWarming_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{beamWarming_discontinuous.png} \\
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{fromm_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{fromm_discontinuous.png} 

\end{tabular}
\end{figure}

\newpage

\begin{figure}[H]
\centering
\begin{tabular}{cc}
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_lw_continuous.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_lw_discontinuous.png}
\end{tabular}
\end{figure}

The graphic on the left shows the global error, computed using the $L_1$ norm at each interior grid node, for the upwind and lax-wendroff methods for varying grid resolutions for the continuously varying sine wave advection function. On the right we see the error for the same methods and same grids, but for the discontinuous shock case. What we see is that unlike in the continuous case, we are no longer able to observe the difference in error between the two methods due the facts that the upwind method is known to converge in first-order, and that the lax-wendroff is known to converge in second-order. I hypothesize that we do not see this difference for the discontinuous case because most of our approximated points are lying directly along either $1$ or $-1$, and thus the approximation error for each of the points that are not near the shock becomes negligible in trying to find a difference in the ability to accurately approximate the PDE for the two methods. As can be understood by observing the correspond figures on the prior page, however, the two methods do not produce the same global error as their behavior near the shock differs, with the deviation between the methods appearing to remain relatively constant as the grid resolution varies. 
\newline\newline
On the next page we will discuss the following four graphs, which correspond to the initial conditions given section 8.1 of Jiang and Shu (JCP, 126, pp. 202-228 (1996)). 

\begin{figure}[H]
\centering
\begin{tabular}{cc}
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_c04_t8.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{upwind_c09_t8.png} \\
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxWendroff_c04_t8.png} &
\includegraphics[width=0.5\textwidth, height=0.2\textheight]{laxWendroff_c09_t8.png}
\end{tabular}
\end{figure}

\newpage
As observed on the previous page, the lax-wendroff method seems to outperform the upwind method by a significant margin for both values of the CFL parameter $C$. The upwind method fails to preserve the shape of the peaks and values for $C = 0.4$, but appears to still have some notion of the original distribution when $C=0.9$. It is not surprising that this be the case. Since the size of time step is scaled by the constant $C$, the number of steps in the simulation to get to the same time $tMax$ in two different simulations is more for a simulation with a lower value of $C$. More iterations in the simulation could mean more local errors contributing to the global error. Likewise, even the relatively better performing lax-wendroff method undergoes much larger oscillations for $C=0.4$ than it does when $C=0.9$. In comparison to the WENO methods and Runge-Kutta methods employed in figure 2 of Jiang and Shu's paper, both of these methods lead to poor approximations. It is clear that some higher-order methods and/or more sophisticated techniques will need to be added to the model if we wish to approximate PDE's reasonable accuracy for longer time durations.

\section*{CONCLUSION}
In conclusion, this report showed how linear scalar advection can be performed. A variety of techniques were discussed and implemented in Fortran 90 to do this for both continuous sine wave advection, and discontinuous shock advection. Of the five methods for which we plotted the numerical approximation along with the exact solutions on page 3, we see that all methods approximate the evolution of the continuous system well for one period of the sine wave. However, the methods behave quite differently in the presence of a shock wave. In this case we observe dissipation and oscillations near the shock. Additionally, we looked at a different initial condition and saw that we need to improve upon these models to model more complicated initial conditions for longer time durations. 

\end{document}





