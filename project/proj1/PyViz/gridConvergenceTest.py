import matplotlib.pyplot as plt
import numpy as np
import math

# THESE MUST MATCH WHAT IS IN advect.f90
method = 'upwind'
N = 256
xa = 0.0
xb = 1.0
dx = (xb - xa) / N
C = 0.9 # Courant number for CFL condition
continuous = False
a = 1.0 # constant advection velocity
dt = C * dx * (1/abs(a))
M = 88
write_every = math.floor(M/4.0)

data_to_plot = []

filename = '../PDE/data/output_{0}_time{1}.txt'.format(method, M)
filename = '%r'%filename
filename = filename[1:-1]
print("\t READING FROM: {0}".format(filename))

file_object = open(filename, 'r')
method_name = filename.split('.')[2].split('_')[1]

# current iteraton of simulation - used to calculate time
it = int(filename.split('.')[2].split('time')[-1])

binary = False

if binary:
        pass

else: 
        data = file_object.readlines()
        data = np.fromstring(' '.join(data[1:]), dtype=float, sep=' ')
        x = []
        x.append(xa - 1.5*dx)
        for i in range(1, len(data[1:])):
            x.append(x[i-1] + dx)

        data_to_plot.append(data[1:])

global_error = 0

if continuous:
        # 2 ghost cells on either side
        for i, x_i in enumerate(x):
                # only check interior nodes
                if (x_i >= 0) or (x_i <= 1):
                        global_error += dx * abs(math.sin(2 * math.pi * x_i) - data[i])
else:
         for i, x_i in enumerate(x):
                if (x_i >= 0) or (x_i <= 1):
                        if x_i < 0.8:
                                global_error += dx * abs(1.0 - data[i])
                        else:
                                global_error += dx * abs(-1.0 - data[i])

print("dx: {0} | error {1}".format(dx, global_error))



