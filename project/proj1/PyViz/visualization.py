import matplotlib.pyplot as plt
import numpy as np
import math

def G(x, beta, z):
        return math.exp(-beta*(x - z)**2)
def F(x, alpha, a):
        return math.sqrt(max(1 - (alpha**2)*(x-a)**2, 0))

# THESE MUST MATCH WHAT IS IN advect.f90
method = 'upwind'
N = 200
xa = -1.0
xb = 1.0
dx = (xb - xa) / N
C = 0.9 # Courant number for CFL condition
continuous = 2
a = 1.0 # constant advection velocity
dt = C * dx * (1/abs(a))
M = 890
write_every = math.floor(M/5.0)

data_to_plot = []

iters = [1]
i=write_every
while i <= M+1:
        iters.append(i)
        i+=write_every

for i in iters: #range(write_every, M+1, write_every):
        filename = '../PDE/data/output_{0}_time{1}.txt'.format(method, i)
        filename = '%r'%filename
        filename = filename[1:-1]
        print("\t READING FROM: {0}".format(filename))

        file_object = open(filename, 'r')
        method_name = filename.split('.')[2].split('_')[1]

        # current iteraton of simulation - used to calculate time
        it = int(filename.split('.')[2].split('time')[-1])

        binary = False

        if binary:
                pass

        else: 
                data = file_object.readlines()
                data = np.fromstring(' '.join(data[1:]), dtype=float, sep=' ')
                x = []
                x.append(xa - 1.5*dx)
                for i in range(1, len(data[1:])):
                    x.append(x[i-1] + dx)

                data_to_plot.append(data[1:])

for time in range(len(data_to_plot)):
        if time == 0:
                pass 
                #plt.plot(x, data_to_plot[time], 'b.', label='$t_0$={}*$\Delta$t'.format(time))
        elif time == len(data_to_plot)-1:
                plt.plot(x, data_to_plot[time], 'r.', label='$t_n$={0}$\Delta$t={1}'.format(M, round(dt*M,5))) 
        else:
                pass 
                #plt.plot(x, data_to_plot[time], 'k--', label='t={}*$\Delta$t'.format(time*write_every))  

if continuous == 1:
        exact = np.sin(np.array( [2*math.pi*i for i in x] ))

elif continuous == 2:
        # CONSTANTS
        delta = 0.005
        z = -0.7
        beta = math.log(2)/(36*delta**2)
        a = 0.5
        alpha = 10

        exact = []

        #print *, "IN THE FUNC"
        for x_i in x: 
        
                if ((x_i >= -0.8) and (x_i <= -0.6)):
                        tmp1 = G(x_i, beta, z - delta)
                        tmp2 = G(x_i, beta, z + delta)
                        tmp3 = G(x_i, beta, z)
                        exact.append((1.0/6.0) * (tmp1 + tmp2 + 4*tmp3))
                elif ((x_i >= -0.4) and (x_i <= -0.2)):
                        exact.append(1.0)
                elif ((x_i >= 0.0) and (x_i <= 0.2)):
                        exact.append(1.0 - abs(10.0 * (x_i - 0.1)))
                elif ((x_i >= 0.4) and (x_i <= 0.6)):
                        tmp1 = F(x_i, alpha, a - delta)
                        tmp2 = F(x_i, alpha, a + delta)
                        tmp3 = F(x_i, alpha, a)
                        exact.append((1.0/6.0) * (tmp1 + tmp2 + 4*tmp3))
                else:
                        exact.append(0.0)
        
else:
        exact = np.array( [1.0 if i < 0.8 else -1.0 for i in x] )
plt.plot(x, exact, 'k-', label='exact')

plt.tick_params(axis='both', which='both', top=False, left=False)
plt.title("{0} method with C = {1}, tMax = {2}".format(method_name, C,round(M*dt, 4)))
plt.xlabel('x')
plt.ylabel('U')
plt.legend()
plt.show()

