import matplotlib.pyplot as plt
import numpy as np

upwind = open('../PyViz/upwind_error_discontinuous.txt', 'r')
lw = open('../PyViz/laxWendroff_error_discontinuous.txt', 'r')

x = []
y_upwind = []
y_lw = []

for l in upwind.readlines()[:-1]:
        #print(line)
        line = l.split()
        x.append(float(line[1]))
        y_upwind.append(float(line[-1]))

for l in lw.readlines()[:-1]:
        line = l.split()
        y_lw.append(float(line[-1]))

print(x)
print([round(i,2) for i in y_upwind])
print([round(i,2) for i in y_lw])


fig, ax = plt.subplots()
ax.loglog(x, y_upwind, 'k--', label='upwind')
ax.loglog(x, y_lw, 'r--',label='lax-wendroff')
#ax.set_xscale('log')
ax.set_xticks([0.001, 0.01, 0.1])
ax.set_yticks([0.01, 0.1, 1])
plt.xlabel('$\Delta x$')
plt.ylabel('Error')
plt.legend()
plt.show()

